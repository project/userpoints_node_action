<?php


// Helper functions for userpoints_node_action.module

/**
 * Admin form for new rules
 */
function userpoints_node_action_admin_add_form() {
	// Get list of node types and roles
	$roles = user_roles(true);
	$pubtypes = node_get_types('names');
	$actions = _userpoints_node_action_get_actions();
	$categories = userpoints_get_categories();
	
	// Build the form
	$form = array(
		'#id' => 'userpoints_node_action_admin_add_form',
		'#redirect' => 'admin/settings/userpoints_node_action',
	);
	$form['add'] = array (
		'#type' => 'fieldset'
	);
	$form['add']['title'] = array (
		'#value' => '<h2>'.t('Add Node Action Rule').'</h2>',
	);
	$form['add']['role'] = array (
		'#type' => 'select',
		'#required' => true,
		'#title' => t('Role'),
		'#options' => $roles
	);
	$form['add']['pubtype'] = array (
		'#type' => 'select',
		'#required' => true,
		'#title' => t('Publication type'),
		'#options' => $pubtypes
	);
	$form['add']['action'] = array(
		'#type' => 'select',
		'#required' => 'true',
		'#title' => t('Action'),
		'#options' => $actions
	);
	$form['add']['category'] = array(
		'#type' => 'select',
		'#required' => 'true',
		'#title' => t('Category'),
		'#options' => $categories
	);
	$form['add']['points'] = array (
		'#type' => 'textfield',
		'#required' => true,
		'#title' => t('!Points', userpoints_translation()),
		'#description' => t('Number of !points it costs to perform the selected action on the selected node for the selected user role.', userpoints_translation()),
		'#maxlength' => 8,
		'#size' => 8,
		'#validate' => array('userpoints_node_action_validate_points' => array()),
	);
	$form['add']['submit'] = array (
		'#type' => 'submit',
		'#value' => t('Add')
	);
	return $form;
}

function userpoints_node_action_admin_modify_form () {
	// Get list of node types and roles, and actual rules
	$roles = user_roles(true);
	$pubtypes = node_get_types('names');
	$actions = _userpoints_node_action_get_actions();
	$categories = userpoints_get_categories();
	$rules = userpoints_node_action_get_all();
	
	// Build the form
	$form = array(
		'#id' => 'userpoints_node_action_admin_modify_form',
		'#redirect' => 'admin/settings/userpoints_node_action',
	);
	$form['modify'] = array (
		'#type' => 'fieldset',
		'#tree' => true,
	);
	$form['modify']['title'] = array (
		'#value' => '<h2>'.t('Stored rules').'</h2>',
	);
	
	// Loop for every stored rule
	$i = 0;
	if(!empty($rules)) {
		foreach ($rules as $rule) {
			$form['modify'][$i]['role'] = array(
				'#type' => 'select',
				'#required' => true,
				'#options' => $roles,
				'#default_value' => $rule['role_id'],
			);
			$form['modify'][$i]['pubtype'] = array(
				'#type' => 'select',
				'#required' => true,
				'#options' => $pubtypes,
				'#default_value' => $rule['pubtype'],
			);
			$form['modify'][$i]['action'] = array(
				'#type' => 'select',
				'#required' => true,
				'#options' => $actions,
				'#default_value' => $rule['action'],
			);
			$form['modify'][$i]['category'] = array(
				'#type' => 'select',
				'#required' => true,
				'#options' => $categories,
				'#default_value' => $rule['category'],
			);
			$form['modify'][$i]['points'] = array(
				'#type' => 'textfield',
				'#required' => true,
				'#maxlength' => 8,
				'#size' => 8,
				'#default_value' => $rule['points'],
				'#validate' => array('userpoints_node_action_validate_points' => array()),
			);
			$form['modify'][$i]['id'] = array(
				'#type' => 'hidden',
				'#value' => $rule['id'],
			);
			$i++;
		}
		
		$form['modify']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Modify'),
			'#weight' => 65535,
			'#tree' => false,
		);
	} else {
		$form['modify']['none'] = array(
			'#value' => t('There are no current rules configured. Add one using the above form.')
		);
	}
	return $form;
}
/**
 * Theme function to render modify rules form in admin side
 */
function theme_userpoints_node_action_admin_modify_form($form) {
	$header = array(t('Role'), t('Publication type'), t('Action'), t('Category'), t('!Points', userpoints_translation()), '');
	$i = 0;
	while ($form['modify'][$i]) {
		$row = array (
			drupal_render($form['modify'][$i]['role']),
			drupal_render($form['modify'][$i]['pubtype']),
			drupal_render($form['modify'][$i]['action']),
			drupal_render($form['modify'][$i]['category']),
			drupal_render($form['modify'][$i]['points']),
			drupal_render($form['modify'][$i]['id']) . l(t('delete'), 'admin/settings/userpoints_node_action/delete/'.$form['modify'][$i]['id']['#value']),
		);
		unset($form['modify'][$i]);
		$rows[] = $row;
		$i++;
	}
	$table = theme('table', $header, $rows);
	$form['modify']['table'] = array (
		'#value' => $table,
		'#weight' => 65535,
	);
	$output = drupal_render_form($form['#id'], $form);
	return $output;
}

/**
 * Get all rules and return as multidimensional array
 */
function userpoints_node_action_get_all() {
	$result = db_query ("SELECT * FROM {userpoints_node_action}");
	$rules = array();
	while ($rule = db_fetch_array($result)) {
		$rules[] = $rule;
	}
	return $rules;
}

/**
 * Get all node actions as array
 */
function _userpoints_node_action_get_actions() {
	return array(
		t('publish') => t('Publish'),
		t('sticky') => t('Sticky'),
		t('promote') => t('Promote')
	);
}

/**
 * Returns an object that holds the cost and category. Since we're only getting one (as well as there
 * should only be one limit per person), multiple roles may cause multiple rules to be applied. If this
 * is the case, the rule that costs the most points will be applied.
 */
function _get_userpoints_node_action_user_cost($user, $nodetype, $action, $category = 0) {
	$i = count($user->roles);
	foreach($user->roles as $key => $role) {
		$addRoles .= 'role_id = '. $key;
		$i--;
		if($i > 0) {
			$addRoles .= ' OR ';
		}
	}
	$q = "SELECT points, category FROM {userpoints_node_action} WHERE pubtype = '$nodetype' AND ($addRoles) AND action = '$action'";
	if($category != 0) {
		$q .= " AND category = $category";
	}
	$q .= " ORDER BY points DESC LIMIT 1";
	$cost = db_fetch_object(db_query($q));
	return $cost;
}

/**
 * Gets a status message based on if the user has enough points
 */
function _get_userpoints_node_action_status($account, $pubtype, $action, &$totalCost) {
	global $user;
	$result = null;
	$cost = _get_userpoints_node_action_user_cost($account, $pubtype, $action);
	if(!empty($cost)) {
		$totalCost += $cost->points;
		$points = userpoints_get_current_points($account->uid, $cost->category);
		if($points < $totalCost) {
			// Is it the current user or an admin editing?
			if($user->uid == $account->uid) {
				$result = t("You do not have enough !points to $action this item", userpoints_translation());
			} else {
				$result = t("This user does not have enough !points to $action this item", userpoints_translation());
			}
			if($points >= $cost->points) {
				$result .= t(" (after previous actions, but you may have enough for this action alone)");
			}
		}
	}
	return $result;
}

/**
 * Makes a user pay for their actions (sounds a little ominous)
 */
function _userpoints_node_action_payment($account, $pubtype, $action) {
	global $user;
	$result = null;
	$cost = _get_userpoints_node_action_user_cost($account, $pubtype, $action);
	if(!empty($cost)) {
		$points = userpoints_get_current_points($account->uid, $cost->category);
		// They don't have enough points, even though we calculated it before...
		if($points < $cost->points) {
			$result = array('status' => false, 'reason' => t("Not enough !points to $action this item", userpoints_translation()));
		} else {
			$params = array('uid' => intval($account->uid), 'points' => ($cost->points * -1), 'tid' => intval($cost->category), 'display' => true, 'operation' => 'node_action', 'description' => t("!Points cost for action: \"$action\" on a node of type: \"$pubtype\"", userpoints_translation()));
			$result = userpoints_userpointsapi($params);
		}
	}
	return $result;
}

/**
 * Checks to see if the new action status has previously been set. For instance, if
 * a node was published, then simply edited without changing the publish status, the
 * user shouldn't be charged for it again
 */
function _userpoints_node_action_has_status_changed($nid, $action, $newStatus = 1) {
	// It seems the only reliable way to do this is to reload the node. It could be a direct
	// query to the node table, but node_load would be more resistant to change.
	// If the nid is empty, it's because they're making a new node
	if(empty($nid)) {
		return true;
	}
	$node = node_load($nid);
	switch($action) {
		case "publish":
			$property = "status";
			break;
		case "sticky":
			$property = "sticky";
			break;
		case "promote":
			$property = "promote";
			break;
		default:
			$property = null;
			break;
	}
	// Oops? Must be a problem, return true by default;
	if(empty($property) || empty($node)) {
		return true;
	}
	// If the statuses are different
	if($node->$property != $newStatus) {
		return true;
	} else {
		return false;
	}
}